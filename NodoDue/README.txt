/**************************************************************************\

    This project, with all files, are part of the NodoDue Manager
    All files, except mentioned below, \u00a9 Copyright M. van Leeuwen

    gnu.io.rxtx : See lib\License GnuRXTX.txt
    jcalendar   : \u00a9 Copyright Tony Freixas
    jexcelapi   : \u00a9 Copyright Andy Khan (LGPL)

    NodoDue Manager is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NodoDue Manager is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NodoDue Manager.  If not, see <http://www.gnu.org/licenses/>.

\**************************************************************************/

For developers, check the support folder.

