/*
 * NodoDueManagerAboutBox.java
 */

package nl.lemval.nododue;

import com.connectina.swing.fontchooser.JFontChooser;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.jdesktop.application.Action;

public class NodoDueManagerOptionBox extends javax.swing.JDialog {

    public NodoDueManagerOptionBox(java.awt.Frame parent) {
        super(parent);
        initComponents();
        getRootPane().setDefaultButton(closeButton);
        KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        getRootPane().registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        }, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

        font2Text.setVisible(false);
        font2Label.setVisible(false);
    }

    @Override
    public void setVisible(boolean b) {
        if ( b ) {
            Options options = Options.getInstance();
            autoDetect.getModel().setSelected(options.isNodoUnitAutoScan());
            font1Text.setText(Options.getFontString(options.getBaseFont()));
            font2Text.setText(Options.getFontString(options.getTitleFont()));
        }
        super.setVisible(b);
    }

    @Action
    public void closeOptionBox() {
        Options options = Options.getInstance();
        options.setNodoUnitAutoScan(autoDetect.getModel().isSelected());
        dispose();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.JLabel imageLabel = new javax.swing.JLabel();
        javax.swing.JLabel appTitleLabel = new javax.swing.JLabel();
        javax.swing.JLabel appDescLabel = new javax.swing.JLabel();
        javax.swing.JLabel autoDetectLabel = new javax.swing.JLabel();
        autoDetect = new javax.swing.JCheckBox();
        javax.swing.JLabel autoConnectLabel = new javax.swing.JLabel();
        autoConnect = new javax.swing.JCheckBox();
        javax.swing.JLabel font1Label = new javax.swing.JLabel();
        font1Text = new javax.swing.JLabel();
        font2Label = new javax.swing.JLabel();
        font2Text = new javax.swing.JLabel();
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(nl.lemval.nododue.NodoDueManager.class).getContext().getResourceMap(NodoDueManagerOptionBox.class);
        setTitle(resourceMap.getString("title")); // NOI18N
        setModal(true);
        setName("aboutBox"); // NOI18N
        setResizable(false);

        imageLabel.setIcon(resourceMap.getIcon("imageLabel.icon")); // NOI18N
        imageLabel.setName("imageLabel"); // NOI18N

        appTitleLabel.setFont(appTitleLabel.getFont().deriveFont(appTitleLabel.getFont().getStyle() | java.awt.Font.BOLD, appTitleLabel.getFont().getSize()+4));
        appTitleLabel.setText(resourceMap.getString("Application.title")); // NOI18N
        appTitleLabel.setName("appTitleLabel"); // NOI18N

        appDescLabel.setText(resourceMap.getString("appDescLabel.text")); // NOI18N
        appDescLabel.setName("appDescLabel"); // NOI18N

        autoDetectLabel.setFont(autoDetectLabel.getFont().deriveFont(autoDetectLabel.getFont().getStyle() | java.awt.Font.BOLD));
        autoDetectLabel.setText(resourceMap.getString("autoDetectLabel.text")); // NOI18N
        autoDetectLabel.setName("autoDetectLabel"); // NOI18N

        autoDetect.setText(resourceMap.getString("autoDetect.text")); // NOI18N
        autoDetect.setName("autoDetect"); // NOI18N

        autoConnectLabel.setFont(autoConnectLabel.getFont().deriveFont(autoConnectLabel.getFont().getStyle() | java.awt.Font.BOLD));
        autoConnectLabel.setText(resourceMap.getString("autoConnectLabel.text")); // NOI18N
        autoConnectLabel.setEnabled(false);
        autoConnectLabel.setName("autoConnectLabel"); // NOI18N

        autoConnect.setText(resourceMap.getString("autoConnect.text")); // NOI18N
        autoConnect.setEnabled(false);
        autoConnect.setName("autoConnect"); // NOI18N

        font1Label.setFont(font1Label.getFont().deriveFont(font1Label.getFont().getStyle() | java.awt.Font.BOLD));
        font1Label.setText(resourceMap.getString("font1Label.text")); // NOI18N
        font1Label.setName("font1Label"); // NOI18N

        font1Text.setText(resourceMap.getString("font1Text.text")); // NOI18N
        font1Text.setName("font1Text"); // NOI18N
        font1Text.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                font1Clicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                font1Clicked(evt);
            }
        });

        font2Label.setFont(font2Label.getFont().deriveFont(font2Label.getFont().getStyle() | java.awt.Font.BOLD));
        font2Label.setText(resourceMap.getString("font2Label.text")); // NOI18N
        font2Label.setName("font2Label"); // NOI18N

        font2Text.setText(resourceMap.getString("font2Text.text")); // NOI18N
        font2Text.setName("font2Text"); // NOI18N
        font2Text.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                font2Clicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                font2Clicked(evt);
            }
        });

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(nl.lemval.nododue.NodoDueManager.class).getContext().getActionMap(NodoDueManagerOptionBox.class, this);
        closeButton.setAction(actionMap.get("closeOptionBox")); // NOI18N
        closeButton.setText(resourceMap.getString("closeButton.text")); // NOI18N
        closeButton.setName("closeButton"); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(imageLabel)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(appTitleLabel)
                    .add(appDescLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(autoConnectLabel)
                            .add(autoDetectLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 97, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(font1Label)
                            .add(font2Label))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(font2Text)
                            .add(font1Text)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(autoDetect, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                                .add(autoConnect, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, closeButton)))))
                .addContainerGap(10, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(imageLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 182, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(appTitleLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(appDescLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(autoDetectLabel)
                    .add(autoDetect))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(autoConnectLabel)
                    .add(autoConnect))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(font1Text)
                    .add(font1Label))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(font2Label)
                    .add(font2Text))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 9, Short.MAX_VALUE)
                .add(closeButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void font2Clicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_font2Clicked
        Options opts = Options.getInstance();
        Font f = opts.getTitleFont();
        f = selectFont(f);
        if ( f != null ) {
            opts.setTitleFont(f);
            font2Text.setText(Options.getFontString(f));
        }
    }//GEN-LAST:event_font2Clicked

    private void font1Clicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_font1Clicked
        Options opts = Options.getInstance();
        java.awt.Font f = opts.getBaseFont();
        f = selectFont(f);
        if ( f != null ) {
            opts.setBaseFont(f);
            font1Text.setText(Options.getFontString(f));
        }
    }//GEN-LAST:event_font1Clicked

    private Font selectFont(Font f) {
        return JFontChooser.showDialog(this, f);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox autoConnect;
    private javax.swing.JCheckBox autoDetect;
    private javax.swing.JButton closeButton;
    private javax.swing.JLabel font1Text;
    private javax.swing.JLabel font2Label;
    private javax.swing.JLabel font2Text;
    // End of variables declaration//GEN-END:variables

}
